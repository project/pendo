<?php

namespace Drupal\Tests\pendo\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group pendo
 */
class LoadTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['pendo'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests that Pendo is active in JS settings.
   */
  public function testSettings() {
    $this->drupalPostForm('admin/config/services/pendo', ['api_key' => '1234'], t('Save configuration'));

    $pendo = $this->getJsSettings()['pendo'];
    $this->assertSame('1234', $pendo['api_key']);
    $this->assertNotEmpty($pendo['data']['account']['id']);
    $this->assertNotEmpty($pendo['data']['visitor']['id']);
    $this->assertNotEmpty($pendo['data']['visitor']['email']);
  }

  /**
   * Returns the JavaScript drupalSettings object.
   *
   * @return array
   *   The decoded array of JavaScript settings.
   */
  private function getJsSettings() {
    $settings = $this->assertSession()
      ->elementExists('css', '[data-drupal-selector="drupal-settings-json"]')
      ->getText();

    return Json::decode($settings);
  }

}
